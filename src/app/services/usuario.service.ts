import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario.models';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private url = 'https://reqres.in/api';
  constructor(private http: HttpClient) { }

  getUsers(): Observable<Usuario[]> {
    return this.http.get(`${this.url}/users?page=1&delay=11`)
      .pipe(map(resp => resp['data']));
  }

  getUserById(id: string): Observable<Usuario> {
    return this.http.get(`${this.url}/users/${id}`)
      .pipe(
        map(resp => resp['data'])
      );
  }
}
