import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as usuariosActions from '../actions/usuarios.actions';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import { UsuarioService } from 'src/app/services/usuario.service';
import { of } from 'rxjs';

@Injectable()
export class UsuariosEffects {

    constructor(
        // Observable escuchando las acciones que se disparan
        private actions$: Actions,
        public usuarioService: UsuarioService
    ) {

    }
    cargarUsuarios$ = createEffect(
        //sin pipe se dispararía para cualquier acción, entonces pongo pipe para disparar sólo cuando se llame a cargarUsuarios.
        () => this.actions$.pipe(
            ofType(usuariosActions.cargarUsuarios),
            // tap(data => console.log('effect tap', data)),
            // Ayuda a disparar un nuevo observable y juntarlo al anterior
            mergeMap(
                () => this.usuarioService.getUsers()
                    .pipe(
                        // tap(data => console.log('getUsers effect', data)),
                        map(users => usuariosActions.cargarUsuariosSuccess({ usuarios: users })),
                        catchError(err => of(usuariosActions.cargarUsuariosError({ payload: err })))
                    )
            )
        )
    );
}
